﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FSharpTTTNuget;
using TTTWebApi.Dto;
using TTTWebApi.Implementation;
using Xunit;

namespace TTTWebApi.Test.Implementation
{
    public class GameManagerTest
    {
        [Fact (DisplayName = "ValidateMove() If the given cell is already taken it returns false with proper message")]
        public async Task ValidateMove_GivenCellTaken_ReturnsFalse()
        {
            var board = TicTacToe.createBoard(3);
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 3, "X")),
                CurrentToken = "O",
                OpponentToken = "X",
                Move = 4
            };
            var gameManager = new GameManager();

            Assert.Equal(new List<string>{TicTacToe.CellTaken}, await gameManager.ValidateMove(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateMove() If the move is < 1 it returns false with proper message")]
        public async Task ValidateMove_MoveLessThanOne_ReturnsFalse()
        {
            var board = TicTacToe.createBoard(3);
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 0
            };
            var gameManager = new GameManager();

            Assert.Equal(new List<string>{TicTacToe.InvalidInput}, await gameManager.ValidateMove(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateMove() If the move is > board size it returns false with proper message")]
        public async Task ValidateMove_MoveGreaterThanBoardSize_ReturnsFalse()
        {
            var board = TicTacToe.createBoard(3);
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 10
            };
            var gameManager = new GameManager();

            Assert.Equal(new List<string>{TicTacToe.InvalidInput}, await gameManager.ValidateMove(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateMove() If the request object is valid, it returns an empty list")]
        public async Task ValidateMove_ValidRequestObject_ReturnsMove()
        {
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 3
            };
            
            var gameManager = new GameManager();
            
            Assert.Equal(new List<string>(), await gameManager.ValidateMove(validRequest));
        }
        
        [Fact (DisplayName = "MakeMove() given a valid move, it returns proper response")]
        public async Task MakeMove_GivenValidMove_ReturnsProperResponse()
        {
            var board = TicTacToe.createBoard(3);
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 3, "X")),
                GameStatus = TicTacToe.GameStatus.Ongoing
            };
            
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 4
            };
            
            var gameManager = new GameManager();

            var response = await gameManager.MakeMove(validRequest);
            
            Assert.Equal(expectedResponse.Board, response.Board);
            Assert.Equal(expectedResponse.GameStatus, response.GameStatus);
        }
        
        [Fact (DisplayName = "MakeMove() given a move that would win, it returns proper status")]
        public async Task MakeMove_GivenWinningMove_ReturnsProperStatus()
        {
            var board = TicTacToe.createBoard(3);
            board = TicTacToe.buildBoardFromExisting(board, 0, "X");
            board = TicTacToe.buildBoardFromExisting(board, 1, "X");
            board = TicTacToe.buildBoardFromExisting(board, 3, "O");
            board = TicTacToe.buildBoardFromExisting(board, 6, "O");
            
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 2, "X")),
                GameStatus = TicTacToe.GameStatus.Win
            };
            
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 3
            };
            
            var gameManager = new GameManager();

            var response = await gameManager.MakeMove(validRequest);
            
            Assert.Equal(expectedResponse.Board, response.Board);
            Assert.Equal(expectedResponse.GameStatus, response.GameStatus);
        }

        [Fact(DisplayName = "MakeMove() given a move that would tie, it returns proper status")]
        public async Task MakeMove_GivenTieMove_ReturnsProperStatus()
        {
            var board = TicTacToe.createBoard(3);
            board = TicTacToe.buildBoardFromExisting(board, 0, "X");
            board = TicTacToe.buildBoardFromExisting(board, 1, "X");
            board = TicTacToe.buildBoardFromExisting(board, 4, "X");
            board = TicTacToe.buildBoardFromExisting(board, 5, "X");
            board = TicTacToe.buildBoardFromExisting(board, 6, "X");
            board = TicTacToe.buildBoardFromExisting(board, 2, "O");
            board = TicTacToe.buildBoardFromExisting(board, 3, "O");
            board = TicTacToe.buildBoardFromExisting(board, 7, "O");

            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 8, "O")),
                GameStatus = TicTacToe.GameStatus.Tie
            };

            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "O",
                OpponentToken = "X",
                Move = 9
            };

            var gameManager = new GameManager();

            var response = await gameManager.MakeMove(validRequest);

            Assert.Equal(expectedResponse.Board, response.Board);
            Assert.Equal(expectedResponse.GameStatus, response.GameStatus);
        }

        [Fact (DisplayName = "GetComputerMove() given a valid request, it returns the next move for the computer")]
        public async Task GetComputerMove_GivenValidRequest_ReturnsComputerMove()
        {
            var board = TicTacToe.createBoard(3);
            board = TicTacToe.buildBoardFromExisting(board, 3, "X");
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "O",
                OpponentToken = "X",
                Move = null
            };
            
            var gameManager = new GameManager();
            
            Assert.Equal(7, await gameManager.GetComputerMove(validRequest));
        }

        [Fact(DisplayName = "MapToEnumerableString() given an FSharp list of cells with numbers, returns proper list of strings")]
        public void MapToEnumerableString_GivenValidFSharpList_ReturnsStringList()
        {
            var expectedList = new List<string>
            {
                "X",
                "2",
                "3",
                "O",
                "X",
                "6",
                "7",
                "8",
                "O"
            };

            var cellList = TicTacToe.createBoard(3);
            cellList = TicTacToe.buildBoardFromExisting(cellList, 0, "X");
            cellList = TicTacToe.buildBoardFromExisting(cellList, 3, "O");
            cellList = TicTacToe.buildBoardFromExisting(cellList, 4, "X");
            cellList = TicTacToe.buildBoardFromExisting(cellList, 8, "O");

            Assert.Equal(expectedList, GameManager.MapToEnumerableString(cellList).ToList());
        }

        [Fact(DisplayName = "MapToFSharpListCell() given a list of strings with numbers, returns proper FSharp list of cells")]
        public void MapToFSharpListCell_GivenValidStringList_ReturnsFSharpCellList()
        {
            var expectedList = TicTacToe.createBoard(3);
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 0, "X");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 3, "O");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 4, "X");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 8, "O");
            
            var stringList = new List<string>
            {
                "X",
                "2",
                "3",
                "O",
                "X",
                "6",
                "7",
                "8",
                "O"
            };

            Assert.Equal(expectedList, GameManager.MapToFSharpListCell(stringList));
        }
        
        [Fact(DisplayName = "MapToFSharpListCell() given a list with empty strings, returns proper FSharp list of cells")]
        public void MapToFSharpListCell_GivenValidStringListWithEmpty_ReturnsFSharpCellList()
        {
            var expectedList = TicTacToe.createBoard(3);
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 0, "X");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 3, "O");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 4, "X");
            expectedList = TicTacToe.buildBoardFromExisting(expectedList, 8, "O");
            
            var stringList = new List<string>
            {
                "X",
                "",
                "",
                "O",
                "X",
                "",
                "",
                "",
                "O"
            };

            Assert.Equal(expectedList, GameManager.MapToFSharpListCell(stringList));
        }
    }
}