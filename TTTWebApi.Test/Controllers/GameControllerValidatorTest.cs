﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FSharpTTTNuget;
using Microsoft.FSharp.Collections;
using TTTWebApi.Controllers;
using TTTWebApi.Dto;
using TTTWebApi.Implementation;
using Xunit;

namespace TTTWebApi.Test.Controllers
{
    public class GameControllerValidatorTest
    {
        [Fact (DisplayName = "ValidateRequest() If the board is empty it returns false with proper message")]
        public async Task ValidateRequest_NullBoard_ReturnsFalse()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = null,
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 1
            };
            
            Assert.Equal(new List<string>{GameControllerValidator.BoardRequired},
                await GameControllerValidator.ValidateRequest(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateRequest() If the board is empty it returns false with proper message")]
        public async Task ValidateRequest_GivenEmptyBoard_ReturnsFalse()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(FSharpList<TicTacToe.Cell>.Empty),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 1
            };

            Assert.Equal(new List<string>{GameControllerValidator.BoardRequired},
                await GameControllerValidator.ValidateRequest(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateRequest() If the currentToken is empty it returns false with proper message")]
        public async Task ValidateRequest_NullCurrentToken_ReturnsFalse()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = null,
                OpponentToken = "O",
                Move = 1
            };
            
            Assert.Equal(new List<string>{GameControllerValidator.CurrentTokenRequired},
                await GameControllerValidator.ValidateRequest(invalidRequest));
        }
        
        [Fact (DisplayName = "ValidateRequest() If the currentToken is empty it returns false with proper message")]
        public async Task ValidateRequest_NullOpponentToken_ReturnsFalse()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = "X",
                OpponentToken = null,
                Move = 1
            };
            
            Assert.Equal(new List<string>{GameControllerValidator.OpponentTokenRequired},
                await GameControllerValidator.ValidateRequest(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateRequest() If the currentToken is empty and the opponentToken is empty returns false with combined message")]
        public async Task ValidateRequest_InvalidCurrentTokenAndMove_ReturnsFalse()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = "",
                OpponentToken = null,
                Move = 0
            };
            
            Assert.Equal(new List<string>{GameControllerValidator.CurrentTokenRequired, GameControllerValidator.OpponentTokenRequired},
                await GameControllerValidator.ValidateRequest(invalidRequest));
        } 
        
        [Fact (DisplayName = "ValidateRequest() If the request object is empty it returns false with proper message")]
        public async Task ValidateRequest_NullRequestObject_ReturnsFalse()
        {
            Assert.Equal(new List<string>{GameControllerValidator.RequestObjectRequired}, 
                await GameControllerValidator.ValidateRequest(null));
        } 
    }
}