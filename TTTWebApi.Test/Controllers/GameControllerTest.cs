﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FSharpTTTNuget;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using TTTWebApi.Abstraction;
using TTTWebApi.Controllers;
using TTTWebApi.Dto;
using TTTWebApi.Implementation;
using Xunit;

namespace TTTWebApi.Test.Controllers
{
    public class GameControllerTest
    {
        [Fact (DisplayName = "PostGame() Verify that the request validation is called and returns false with error message")]
        public async Task PostGame_ValidationIsCalled_ReturnsBadRequest()
        {
            var invalidRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = -1
            };
            var gameMock = new Mock<IGame>();

            gameMock.Setup(x => x.ValidateMove(invalidRequest))
                .ReturnsAsync(new List<string>{GameControllerValidator.CurrentTokenRequired});

            var controller = new GameController(gameMock.Object);
            var result = await controller.PostGame(invalidRequest) as BadRequestObjectResult;
            
            Assert.Equal(StatusCodes.Status400BadRequest, result?.StatusCode);
            gameMock.Verify(x => x.ValidateMove(invalidRequest), Times.Once);
        }
        
        [Fact (DisplayName = "PostGame() Verify that the request validation is called and returns OK result")]
        public async Task PostGame_ValidationIsCalled_ReturnsOk()
        {
            var board = TicTacToe.createBoard(3);
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 2, "X")),
                GameStatus = TicTacToe.GameStatus.Ongoing
            };
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.createBoard(3)),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 3
            };
            var gameMock = new Mock<IGame>();

            gameMock.Setup(x => x.ValidateMove(validRequest)).ReturnsAsync(new List<string>());
            gameMock.Setup(x => x.MakeMove(validRequest)).ReturnsAsync(expectedResponse);

            var controller = new GameController(gameMock.Object);
            var result = await controller.PostGame(validRequest) as OkObjectResult;
            
            Assert.Equal(StatusCodes.Status200OK, result?.StatusCode);
            gameMock.Verify(x => x.ValidateMove(validRequest), Times.Once);
            gameMock.Verify(x => x.MakeMove(validRequest), Times.Once);
            gameMock.Verify(x => x.GetComputerMove(validRequest), Times.Never);
        }
        
        [Fact (DisplayName = "PostGame() If the move is not present, generate a computer move")]
        public async Task PostGame_IfMoveIsNotPresent_CallsGetComputerMove()
        {
            var board = TicTacToe.createBoard(3);
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 3, "X")),
                GameStatus = TicTacToe.GameStatus.Ongoing
            };
            var validRequest = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "O",
                OpponentToken = "X",
                Move = null
            };
            var gameMock = new Mock<IGame>();

            gameMock.Setup(x => x.ValidateMove(validRequest)).ReturnsAsync(new List<string>());
            gameMock.Setup(x => x.MakeMove(validRequest)).ReturnsAsync(expectedResponse);

            var controller = new GameController(gameMock.Object);
            var result = await controller.PostGame(validRequest) as OkObjectResult;
            
            Assert.Equal(StatusCodes.Status200OK, result?.StatusCode);
            gameMock.Verify(x => x.ValidateMove(validRequest), Times.Once);
            gameMock.Verify(x => x.MakeMove(validRequest), Times.Once);
            gameMock.Verify(x => x.GetComputerMove(validRequest), Times.Once);
        }
    }
}