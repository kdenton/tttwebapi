# README #

Welcome to the documentation for my Tic-Tac-Toe Web API. In here you'll find everything you need to use this API.

### What is this repository for? ###

* This repository is used to provide an API for a web client to interact with an F# Tic-Tac-Toe library with an unbeatable AI that I have built. 
* Version 1.0

### How do I get set up? ###

* First you're going to need make sure that you have .NET Core 2.0.3 or higher installed.
* Next, assuming you have Git installed, you need to clone this repository onto your local environment.
* After that you should be able to simply do a `dotnet restore` command to install the necessary Nuget packages.
* Once all packages have been installed, in a terminal opened in the `./TTTWebApi` directory do the 'dotnet run' command to start the API locally.

### Using the API ###

* This API has one POST endpoint, `http://myURL/api/game`.
* You will need a `Content-Type` request header with a value of `application/json`.
* Next you will need a request body, this endpoint expects a json object with the following properties:
    * Board : List of strings
        * Example : ```["", "", "", "X", "", "", "", "", ""]```
    * Move : Integer of the place on the board you would like to place the current symbol. On a 3 x 3 board the valid `Moves` would be 1 - 9.
        * Example : `1`
    * CurrentToken : String value of a single character to represent the current player.
        * Example : `"X"`
    * OpponentToken : String value of a single character to represent the opposing player.
        * Example : `"O"`
* Example Request Body : 
```
{"Board":["X", "", "O", "X", "", "", "", "", ""],
"Move":null,
"CurrentToken":"O",
"OpponentToken":"X"}
```
* In your preferred HTTP web request tool, make a POST request to the above `http://myURL/api/game`, with `myURL` being the URL to your locally running API server, using the correct `Content-Type` header and request body.

### API Response ###
* OK Response
    * If you successfully sent a valid request to the endpoint, you will get a 200 OK object result back. There are two parts to the response object, you will receive an updated board and a game status. Below is an example of a response you might receive back.
        * Example : ```
        {
            "board": [
                "X",
                "O",
                "O",
                "X",
                "",
                "",
                "",
                "",
                ""
            ],
            "gameStatus": {
                "case": "Ongoing"
            }
        }
        ```
    * The `board` property is the board from the request object with an additional move made.
    * The `gameStatus` property is an object with a `case` property. Below are the valid cases.
        * `Win` : If the last move resulted in a win for the current player.
        * `Tie` : If the last move resulted in a tie.
        * `Ongoing` : If the game is neither won nor tied and is continuing.

* Bad Request Response
    * If the request is not formed properly you may receive a 400 Bad Request response. There are a number of reasons that this can happen. The Bad Request response object will contain an array of strings to give you more detail of the problem. Below is an example of a Bad Request response object.
        * ```
        [
            " Malformed request object, the CurrentToken property cannot be null or empty.",
            " Malformed request object, the OpponentToken property cannot be null or empty."
        ]
        ```


### Who do I talk to? ###

* Kristopher Denton 
* kdenton@agvance.net