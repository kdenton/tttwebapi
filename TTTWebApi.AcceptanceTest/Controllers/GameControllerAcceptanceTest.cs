﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using TTTWebApi.Dto;
using Xunit;
using FSharpTTTNuget;
using TTTWebApi.Controllers;
using TTTWebApi.Implementation;

namespace TTTWebApi.AcceptanceTest.Controllers
{
    public class GameControllerAcceptanceTest
    {
        private readonly HttpClient _httpClient;
        private readonly TestServer _testServer;
        
        public GameControllerAcceptanceTest()
        {
            var builder = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>();
            _testServer = new TestServer(builder);
            _httpClient = _testServer.CreateClient();
        }
        
        [Fact (DisplayName = "PostGame() Returns 400 Bad Request if validation returns false")]
        public async void PostGame_BadGameRequestException_Returns400BadRequest()
        {
            var invalidBoard = new GameRequestDto
            {
                Board = null,
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 1
            };
            var content = new StringContent(JsonConvert.SerializeObject(invalidBoard),
                Encoding.UTF8, "application/json");
            var request = _testServer.BaseAddress + "api/game";
            
            var response = await _httpClient.PostAsync(request, content);
            var responseContent = JsonConvert.DeserializeObject<List<string>>(await response.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.Equal(new List<string>{GameControllerValidator.BoardRequired}, responseContent);
        }

        [Fact (DisplayName = "PostGame() If the request passes validation, returns a 200 OK object result")]
        public async void PostGame_ValidRequest_ReturnsOkObjectResult()
        {
            var board = TicTacToe.createBoard(3);
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 2, "X")),
                GameStatus = TicTacToe.GameStatus.Ongoing
            };

            var invalidMove = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "X",
                OpponentToken = "O",
                Move = 3
            };
            var content = new StringContent(JsonConvert.SerializeObject(invalidMove),
                Encoding.UTF8, "application/json");
            var request = _testServer.BaseAddress + "api/game";
            
            var response = await _httpClient.PostAsync(request, content);
            var responseContent = JsonConvert.DeserializeObject<GameResponseDto>
                (await response.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedResponse.Board, responseContent.Board);
            Assert.Equal(expectedResponse.GameStatus, responseContent.GameStatus);
        }
        
        [Fact (DisplayName = "PostGame() If the move is empty, the computer makes move and returns a 200 OK object result")]
        public async void PostGame_EmptyMove_ReturnsOkObjectResult()
        {
            var board = TicTacToe.createBoard(3);
            board = TicTacToe.buildBoardFromExisting(board, 3, "X");
            var expectedResponse = new GameResponseDto
            {
                Board = GameManager.MapToEnumerableString(TicTacToe.buildBoardFromExisting(board, 6, "O")),
                GameStatus = TicTacToe.GameStatus.Ongoing
            };
            var invalidMove = new GameRequestDto
            {
                Board = GameManager.MapToEnumerableString(board),
                CurrentToken = "O",
                OpponentToken = "X",
                Move = null
            };
            var content = new StringContent(JsonConvert.SerializeObject(invalidMove),
                Encoding.UTF8, "application/json");
            var request = _testServer.BaseAddress + "api/game";
            
            var response = await _httpClient.PostAsync(request, content);
            var responseContent = JsonConvert.DeserializeObject<GameResponseDto>
                (await response.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(expectedResponse.Board, responseContent.Board);
            Assert.Equal(expectedResponse.GameStatus, responseContent.GameStatus);
        }
    }
}