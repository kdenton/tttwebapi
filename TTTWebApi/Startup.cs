﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TTTWebApi.Abstraction;
using TTTWebApi.Implementation;

namespace TTTWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IGame, GameManager>();
            services.AddCors(options =>
            {
                options.AddPolicy("GlobalPolicy",
                    builder => builder
                        .WithOrigins("http://localhost:4200",
                            "https://localhost:4200",
                            "http://tttcli.blog.agvancesky.com",
                            "https://tttcli.blog.agvancesky.com")
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("GlobalPolicy");
            app.UseMvc();
        }
    }
}