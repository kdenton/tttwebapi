﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTTWebApi.Dto;

namespace TTTWebApi.Controllers
{
    public static class GameControllerValidator
    {
        public const string BoardRequired = " Malformed request object, the board array cannot be null or empty.";
        public const string CurrentTokenRequired = " Malformed request object, the CurrentToken property cannot be null or empty.";
        public const string OpponentTokenRequired = " Malformed request object, the OpponentToken property cannot be null or empty.";
        public const string RequestObjectRequired = " The request object cannot be null or empty.";
        
        public static async Task<List<string>> ValidateRequest(GameRequestDto request)
        {
            if (request == null) return await Task.FromResult(new List<string>{RequestObjectRequired});
            if (request.Board == null || !request.Board.Any()) return await Task.FromResult(new List<string>{BoardRequired});

            var errorList = new List<string>();
            if (string.IsNullOrEmpty(request.CurrentToken)) errorList.Add(CurrentTokenRequired);
            if (string.IsNullOrEmpty(request.OpponentToken)) errorList.Add(OpponentTokenRequired);

            return await Task.FromResult(errorList);
        }
    }
}