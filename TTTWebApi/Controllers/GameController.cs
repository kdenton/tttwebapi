﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TTTWebApi.Abstraction;
using TTTWebApi.Dto;

namespace TTTWebApi.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private readonly IGame _game;
        
        public GameController(IGame game)
        {
            _game = game;
        }
        
        [HttpPost]
        public async Task<IActionResult> PostGame([FromBody] GameRequestDto request)
        {
            var errorList = await GameControllerValidator.ValidateRequest(request);
            
            if (errorList.Count > 0)
            {
                return new BadRequestObjectResult(errorList);
            }
            
            if (request.Move == null)
            {
                request.Move = await _game.GetComputerMove(request);
            }
            
             errorList = await _game.ValidateMove(request);
            
            if (errorList.Count > 0)
            {
                return new BadRequestObjectResult(errorList);
            }

            return new OkObjectResult(await _game.MakeMove(request));
        }
    }
}