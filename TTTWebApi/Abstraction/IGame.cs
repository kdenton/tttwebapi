﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TTTWebApi.Dto;

namespace TTTWebApi.Abstraction
{
    public interface IGame
    {
        Task<List<string>> ValidateMove(GameRequestDto request);
        Task<GameResponseDto> MakeMove(GameRequestDto request);
        Task<int> GetComputerMove(GameRequestDto request);
    }
}