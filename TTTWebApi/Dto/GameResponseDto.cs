﻿using System.Collections.Generic;
using FSharpTTTNuget;

namespace TTTWebApi.Dto
{
    public class GameResponseDto
    {
        public IEnumerable<string> Board { get; set; }
        public TicTacToe.GameStatus GameStatus { get; set; }
    }
}