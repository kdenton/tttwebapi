﻿using System.Collections.Generic;

namespace TTTWebApi.Dto
{
    public class GameRequestDto
    {
        public IEnumerable<string> Board { get; set; }
        public int? Move { get; set; }
        public string CurrentToken { get; set; }
        public string OpponentToken { get; set; }
    }
}