﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TTTWebApi.Dto;
using FSharpTTTNuget;
using Microsoft.FSharp.Collections;
using Microsoft.FSharp.Core;
using TTTWebApi.Abstraction;

namespace TTTWebApi.Implementation
{
    public class GameManager : IGame
    {
        public async Task<List<string>> ValidateMove(GameRequestDto request)
        {
            var errorList = new List<string>();
            var validatedMove = TicTacToe.validateMove(MapToFSharpListCell(request.Board), request.Move.ToString());
            
            if (validatedMove.IsInvalid) errorList.Add(((TicTacToe.Move.Invalid) validatedMove).Item);

            return await Task.FromResult(errorList);
        }

        public Task<GameResponseDto> MakeMove(GameRequestDto request)
        {
            var board = TicTacToe.buildBoardFromExisting(MapToFSharpListCell(request.Board), (int) (request.Move - 1), request.CurrentToken);
            var response = new GameResponseDto
            {
                Board = MapToEnumerableString(board),
                GameStatus = TicTacToe.gameIsOver(board, request.CurrentToken, request.OpponentToken)
            };

            return Task.FromResult(response);
        }

        public Task<int> GetComputerMove(GameRequestDto request)
        {
            return Task.FromResult(
                TicTacToe.getComputerNextMove(MapToFSharpListCell(request.Board), request.CurrentToken, request.OpponentToken) + 1);
        }
        
        public static IEnumerable<string> MapToEnumerableString(IEnumerable<TicTacToe.Cell> board)
        {
            return board.Select((x, i) => Equals(x.value, FSharpOption<string>.None) ? (i + 1).ToString() : x.value.Value);
        }

        public static FSharpList<TicTacToe.Cell> MapToFSharpListCell(IEnumerable<string> board)
        {
            var test = board.Select(x => Regex.IsMatch(x, @"^\d+$") || string.Equals(x, string.Empty) ? 
                new TicTacToe.Cell(FSharpOption<string>.None) :
                new TicTacToe.Cell(FSharpOption<string>.Some(x)));

            return ListModule.OfSeq(test);
        }
    }
}